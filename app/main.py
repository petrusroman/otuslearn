import socket

from fastapi import FastAPI

app = FastAPI()


@app.get("/health")
async def health():
    return {"status": "OK"}


@app.get("/hello/{student_name}")
async def read_item(student_name):
    return {"student_name": student_name}


@app.get('/')
async def root():
    return {"hostname": socket.gethostname()}
