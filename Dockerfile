FROM python:3.10-slim

ENV PYTHONUNBUFFERED 1
WORKDIR /app

COPY poetry.lock pyproject.toml ./
COPY . ./

RUN python -m pip install --upgrade pip\
    pip install poetry && \
    poetry config virtualenvs.in-project true && \
    poetry install --no-dev

EXPOSE 8000

CMD poetry run uvicorn --reload --host 0.0.0.0 --port 8000 --loop uvloop app.main:app